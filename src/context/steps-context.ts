import { createContext, Dispatch, SetStateAction } from 'react';

interface StepState {
  step: number;
  setStep: Dispatch<SetStateAction<number>>;
}

export const StepsContext = createContext<StepState>({
  step: 1,
  setStep: function () {},
});
