import { StepView } from './views';
import { StepsProvider } from './provider';
import { useClearLc } from './hooks';

function App() {
  useClearLc();
  return (
    <StepsProvider>
      <StepView />
    </StepsProvider>
  );
}

export default App;
