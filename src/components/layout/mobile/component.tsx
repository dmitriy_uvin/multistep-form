import { MobileLayoutProps } from './types';
import './styles.scss';
import { StepsDots } from '../../common';

export const MobileLayout = ({ children }: MobileLayoutProps) => {
  return (
    <div className='mobile__layout__wrapper'>
      <div className='mobile__header'>
        <StepsDots />
      </div>
      <div className="step__block">
        <div className="mobile__layout__container">
          <div className="step__wrapper">
            {children}
          </div>
        </div>
      </div>
      {/*<div className="controls__block">*/}
      {/*  <button className='controls__prev__btn'>Go Back</button>*/}
      {/*  <button className='controls__next__btn'>Next Step</button>*/}
      {/*</div>*/}
    </div>
  );
};
