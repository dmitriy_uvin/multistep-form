import { ReactNode } from 'react';

export interface MobileLayoutProps {
  children: ReactNode;
}