import { DesktopLayoutProps } from './types';
import './styles.scss';
import { StepsDots } from '../../common';
import { useContext } from 'react';
import { StepsContext } from '../../../context';
import { LAST_STEP } from '../../../config';

export const DesktopLayout = ({ children }: DesktopLayoutProps) => {
  const { step, setStep } = useContext(StepsContext);

  const prevBtnVisible = step > 1;
  const lastBtnVisible = step < LAST_STEP;

  return (
    <div className='desktop__layout__wrapper'>
      <div className="desktop__layout__content">
        <div className="desktop__layout__left">
          <StepsDots mode={'vertical'} />
        </div>
        <div className="desktop__layout__right">
          <div className='desktop__layout__right__content'>
            {children}
          </div>
          {/*<div className="step__controls">*/}
          {/*  <button*/}
          {/*    className='step__controls__prev__btn'*/}
          {/*    onClick={() => setStep(step - 1)}*/}
          {/*    style={{ opacity: prevBtnVisible ? 1 : 0 }}*/}
          {/*    disabled={!prevBtnVisible}*/}
          {/*  >Go Back</button>*/}
          {/*  <button*/}
          {/*    className='step__controls__next__btn'*/}
          {/*    onClick={() => setStep(step + 1)}*/}
          {/*    style={{ opacity: lastBtnVisible ? 1 : 0 }}*/}
          {/*    disabled={!lastBtnVisible}*/}
          {/*  >Next Step</button>*/}
          {/*</div>*/}
        </div>
      </div>
    </div>
  );
};
