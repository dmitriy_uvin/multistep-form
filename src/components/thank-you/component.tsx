import completeIcon from '../../assets/images/icon-thank-you.svg';
import { stepWithTitleAndDesc } from '../common';
import './styles.scss';
import { useEffect } from 'react';
import { FORM_PASSED_KEY } from '../../config';

const StepComponent = () => {
  useEffect(() => {
    localStorage.setItem(FORM_PASSED_KEY, JSON.stringify(true));
  }, []);

  return (
    <div className='thank-you__wrapper'>
      <img src={completeIcon} alt=""/>
    </div>
  );
};

export const ThankYouStep = stepWithTitleAndDesc(<StepComponent />);
