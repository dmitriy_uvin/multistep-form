export interface StepOneFormProps {
  name: string;
  email: string;
  phone: string;
}