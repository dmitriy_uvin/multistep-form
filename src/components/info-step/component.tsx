import './styles.scss';
import { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { StepOneFormProps } from './types';
import { StepWithControls } from '../common/step-with-controls';
import { Header } from '../common/header';
import { INFO_STEP_KEY } from '../../config';

export const StepOne = () => {
  const { register, formState: { errors }, handleSubmit } = useForm<StepOneFormProps>();
  const [defData, setDefData] = useState<StepOneFormProps>({
    name: '',
    email: '',
    phone: '',
  });
  const dataRef = useRef<StepOneFormProps>();

  const formSubmitHandler = async (data: StepOneFormProps) => {
    localStorage.setItem(INFO_STEP_KEY, JSON.stringify(data));
    dataRef.current = data;
  }

  useEffect(() => {
    const data = localStorage.getItem(INFO_STEP_KEY);
    if (data) {
      setDefData({
        ...JSON.parse(data),
      });
    }
  }, []);

  const nextHandler = async (): Promise<boolean> => {
    await handleSubmit(formSubmitHandler)();
    return !Object.keys(errors).length && !!dataRef.current && !!Object.keys(dataRef.current).length;
  }

  return (
    <StepWithControls onNext={nextHandler}>
      <div>
        <Header
          title={'Personal info'}
          description={'Please provide your name, email address, and phone number.'}
        />
        <form>
          <div className="input__group">
            <label htmlFor="name" className={`${errors.name?.message ? 'error': ''}`}>
              <span>Name</span>
              <span className='label__error-text'>{errors.name?.message}</span>
            </label>
            <input
              id="name"
              className={`input__field ${errors.name?.message ? 'input__field--error' : ''}`}
              type="text"
              placeholder='e.g. Stephen King'
              defaultValue={defData.name}
              {...register('name', {
                required: 'Name field is required field',
              })}
            />
          </div>
          <div className="input__group">
            <label htmlFor="email" className={`${errors.email?.message ? 'error': ''}`}>
              <span>Email Address</span>
              <span className='label__error-text'>{errors.email?.message}</span>
            </label>
            <input
              id="email"
              className={`input__field ${errors.email?.message ? 'input__field--error' : ''}`}
              type="email"
              placeholder='e.g. stephenking@lorem.com'
              defaultValue={defData.email}
              {...register('email', {
                required: 'Email field is required field'
              })}
            />
          </div>
          <div className="input__group">
            <label htmlFor="name" className={`${errors.phone?.message ? 'error': ''}`}>
              <span>Phone Number</span>
              <span className='label__error-text'>{errors.phone?.message}</span>
            </label>
            <input
              id="phone"
              className={`input__field ${errors.phone?.message ? 'input__field--error' : ''}`}
              {...register('phone', {
                required: 'Phone Number field is required field'
              })}
              type="text"
              defaultValue={defData.phone}
              placeholder='e.g. +1 234 567 890'
            />
          </div>
        </form>
      </div>
    </StepWithControls>
  );
};

