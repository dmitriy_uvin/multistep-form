import { AddOn, Error } from '../common';
import { AddOnItem } from '../common/addon/types';
import { StepWithControls } from '../common/step-with-controls';
import { Header } from '../common/header';
import { useEffect, useState } from 'react';
import { ADDONS_KEY } from '../../config';

const addOnsItems: AddOnItem[] = [
  {
    id: 1,
    name: 'Online service',
    description: 'Access to multiplayer games',
    price: 1,
  },
  {
    id: 2,
    name: 'Larger storage',
    description: 'Extra 1TB of cloud save',
    price: 2,
  },
  {
    id: 3,
    name: 'Customizable profile',
    description: 'Custom theme on your profile',
    price: 2,
  }
];

export const StepThree = () => {
  const [addons, setAddons] = useState<AddOnItem[]>([]);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    const addonsLC = localStorage.getItem(ADDONS_KEY) ?? '[]';
    const addonsItems = JSON.parse(addonsLC);
    setAddons(addonsItems);
  }, []);

  const addonsIds = () => addons.map((a) => a.id);
  const isSelected = (id: number) => addonsIds().includes(id);
  const nextHandler = async () => {
    if (!addons.length) {
      setIsError(true);
      return false;
    }
    return addons.length > 0;
  }

  const removeAddon = (id: number) => {
    const newItems = [...addons].filter((a) => a.id !== id);
    setAddons([...newItems]);
    saveItems(newItems);
  }

  const clearError = () => {
    if (!addons.length && isError) setIsError(false);
  };

  const saveItems = (items: AddOnItem[]) => {
    localStorage.setItem(ADDONS_KEY, JSON.stringify(items));
  }

  const onSelectAddon = (id: number) => {
    if (isSelected(id)) {
      removeAddon(id);
      return;
    }
    const addon = addOnsItems.find((a) => a.id === id);
    if (!addon) return;
    const newItems = [...addons];
    newItems.push(addon);
    setAddons([...newItems]);
    clearError();
    saveItems(newItems);
  };

  return (
    <StepWithControls onNext={nextHandler}>
      <div>
        <Header title={'Pick add-ons'} description={'Add-ons help enhance your gaming experience.'} />

        {isError && <Error title={'A step is submitted, but no selection has been made'} />}

        {addOnsItems.length > 0 && addOnsItems.map((a) => <AddOn
          key={a.id}
          name={a.name}
          description={a.description}
          price={a.price}
          isSelected={isSelected(a.id)}
          onSelect={() => onSelectAddon(a.id)}
          />
        )}
      </div>
    </StepWithControls>
  );
};

