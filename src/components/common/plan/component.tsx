import { PlanItemProps } from './types';
import './styles.scss';

export const PlanItem = ({ icon, name, onSelect, isSelected, priceRow }: PlanItemProps) => {
  return (
    <div
      className={`plan__item ${isSelected ? 'plan__item--selected': ''}`}
      onClick={onSelect}
    >
      <div className="plan__item__left">
        <img src={icon} alt=""/>
      </div>
      <div className="plan__item__right">
        <p className="plan__item__title">{name}</p>
        <p className="plan__item__price">{priceRow}</p>
      </div>
    </div>
  );
};
