export interface PlanItemProps {
  name: string;
  icon: any;
  onSelect: () => void;
  isSelected: boolean;
  priceRow: string;
}

export type PeriodMode = 'month' | 'year';

export interface Plan {
  id: number;
  name: string;
  image: any;
  yearPrice?: number;
  monthPrice?: number;
}

export interface PlanData {
  id: number;
  name: string;
  image: any;
  price: number;
}