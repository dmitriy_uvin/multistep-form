import { ErrorProps } from './types';
import './styles.scss';

export const Error = (props: ErrorProps) => {
  return (
    <div className='error__text__message'>{props.title}</div>
  );
}