export interface ControlsProps {
  onPrev?: () => void;
  onNext?: () => Promise<boolean>;
}