import { useContext } from 'react';
import { StepsContext } from '../../../context';
import { LAST_STEP } from '../../../config';
import { ControlsProps } from './types';
import './styles.scss';

export const Controls = ({ onPrev, onNext }: ControlsProps) => {
  const { step, setStep } = useContext(StepsContext);

  const prevBtnVisible = step > 1;
  const lastBtnVisible = step <= LAST_STEP;

  const nextBtnTxt = step === LAST_STEP ? 'Confirm' : 'Next Step';

  const nextHandler = async () => {
    if (onNext) {
      const canNext = await onNext();
      if (!canNext) return;
    }
    setStep(step + 1);
  };

  const prevHandler = () => {
    if (onPrev) {
      onPrev();
    }
    setStep(step - 1);
  };

  return (
    <div className="step__controls">
      <button
        className='step__controls__prev__btn'
        onClick={prevHandler}
        style={{ opacity: +prevBtnVisible }}
        disabled={!prevBtnVisible}
      >Go Back</button>
      <button
        type='submit'
        className={`step__controls__next__btn ${step === LAST_STEP ? 'confirm' : ''}`}
        onClick={nextHandler}
        style={{ opacity: +lastBtnVisible }}
        disabled={!lastBtnVisible}
      >
        {nextBtnTxt}
      </button>
    </div>
  );
};
