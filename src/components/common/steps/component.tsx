import { StepDot } from './step-dot';
import { useIsMobile } from '../../../hooks';
import { StepDotsProps } from './types';
import { useContext } from 'react';
import { StepsContext } from '../../../context';

export const StepsDots = ({ mode = 'horizontal' }: StepDotsProps) => {
  const isMobile = useIsMobile();
  const { step } = useContext(StepsContext);

  const steps = [
    {
      name: 'Your info',
      number: 1,
    },
    {
      name: 'Select Plan',
      number: 2,
    },
    {
      name: 'Add-Ons',
      number: 3,
    },
    {
      name: 'Summary',
      number: 4,
    },
  ];
  return (
    <div className={`steps__dots steps__dots--${mode}`}>
      {steps.length > 0 && steps.map((n, index) =>
        <StepDot
          isFull={!isMobile}
          isActive={n.number === step}
          number={n.number}
          name={n.name}
          key={index}
        />
      )}
    </div>
  );
};