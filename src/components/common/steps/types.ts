export interface StepDotProps {
  isActive: boolean;
  number: number;
  name: string;
  isFull?: boolean;
}

export interface StepDotsProps {
  mode?: 'horizontal' | 'vertical';
}
