import { StepDotProps } from './types';
import './styles.scss';

export const StepDot = (
  { isActive, number, name, isFull = false }: StepDotProps
) => {
  return (
    <div className={`step__dot ${isActive ? 'step__dot--active' : ''}`}>
      <div className="step__dot__icon">
        {number}
      </div>
      {isFull && <div className="step__dot__description">
          <div className="step__dot__title">Step {number}</div>
          <div className="step__dot__name">{name}</div>
      </div>}
    </div>
  );
}