export interface AddOnProps {
  name: string;
  description: string;
  price: number;
  isSelected: boolean;
  onSelect: () => void;
}

export interface AddOnItem {
  id: number;
  name: string;
  description: string;
  price: number;
}
