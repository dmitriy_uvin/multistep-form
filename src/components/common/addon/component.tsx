import './styles.scss';
import { AddOnProps } from './types';
import { Checkbox } from '../checkbox';

export const AddOn = (
  { name, description, price, isSelected, onSelect }: AddOnProps
) => {
  return (
    <div
      className={`add-on ${isSelected ? 'add-on--selected' : ''}`}
    >
      <div className="add-on__left">
        <Checkbox
          checked={isSelected}
          onChange={onSelect}
        />
      </div>
      <div className="add-on__right">
        <div className="add-on__content">
          <div className="add-on__name">{name}</div>
          <div className="add-on__description">{description}</div>
        </div>
        <div className="add-on__price">
          ${price}/mo
        </div>
      </div>
    </div>
  );
};