export interface CommonStepProps {
  title: string;
  description: string;
  position?: 'top' | 'bottom';
  textAlign?: 'center'
}