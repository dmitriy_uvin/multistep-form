import { CommonStepProps } from './types';
import './styles.scss';

export const stepWithTitleAndDesc = (component: JSX.Element) => (
  { title, description, position = 'bottom', textAlign }: CommonStepProps
) => {
  const text = <div style={{ textAlign: textAlign ? textAlign : 'unset' }} className={'text__block'}>
    <h1 className='step__title'>{title}</h1>
    <p className='step__subtitle'>{description}</p>
  </div>;

  if (position === 'bottom') {
    return (
      <div>
        {text}
        {component}
      </div>
    );
  }

  return (
    <div>
      {component}
      {text}
    </div>
  );
}