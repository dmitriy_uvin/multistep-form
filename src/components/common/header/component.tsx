import { HeaderProps } from './types';
import './styles.scss';

export const Header = ({ title, description }: HeaderProps) => {
  return (
    <div className={'text__block'}>
      <h1 className='step__title'>{title}</h1>
      <p className='step__subtitle'>{description}</p>
    </div>
  );
}