import { Controls } from '../controls';
import { StepWithControlsProps } from './types';

export const StepWithControls = ({ children, onNext }: StepWithControlsProps) => {
  return (
    <>
      <div>{children}</div>
      <Controls onNext={onNext} />
    </>
  );
};
