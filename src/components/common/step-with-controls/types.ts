import { ReactNode } from 'react';

export interface StepWithControlsProps {
  children: ReactNode;
  onNext: () => Promise<boolean>;
}