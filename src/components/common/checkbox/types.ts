import { ComponentPropsWithRef } from 'react';

export type CheckBoxProps = ComponentPropsWithRef<'input'>;
