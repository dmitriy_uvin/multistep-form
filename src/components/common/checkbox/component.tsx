import './styles.scss';
import { CheckBoxProps } from './types';

export const Checkbox = (props: CheckBoxProps) => {
  return (
    <div className="checkbox__wrapper">
      <label className="checkbox">
        <input type="checkbox" className="checkbox__input" {...props} />
        <span className="checkbox__mark"></span>
      </label>
    </div>
  );
}