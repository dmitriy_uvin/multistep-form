import { Error, stepWithTitleAndDesc } from '../common';
import './styles.scss';
import { PlanItem } from '../common';
import arcadeIcon from '../../assets/images/icon-arcade.svg';
import proIcon from '../../assets/images/icon-pro.svg';
import advancedIcon from '../../assets/images/icon-advanced.svg';
import { useEffect, useState } from 'react';
import { StepWithControls } from '../common/step-with-controls';
import { PeriodMode, Plan } from '../common/plan/types';
import { Header } from '../common/header';
import { PERIOD_MODE_KEY, PLAN_STEP_DATA_KEY, PLAN_STEP_KEY } from '../../config';

const plans: Plan[] = [
  {
    id: 1,
    name: 'Arcade',
    image: arcadeIcon,
    monthPrice: 9,
    yearPrice: 90,
  },
  {
    id: 2,
    name: 'Advanced',
    image: advancedIcon,
    monthPrice: 12,
    yearPrice: 120,
  },
  {
    id: 3,
    name: 'Pro',
    image: proIcon,
    monthPrice: 15,
    yearPrice: 150,
  },
];

export const StepTwo = () => {
  const [plan, setPlan] = useState<Plan>();
  const [mode, setMode] = useState<PeriodMode>('month');
  const [error, setError] = useState<string>('');

  useEffect(() => {
    const planData = localStorage.getItem(PLAN_STEP_KEY);
    if (planData) {
      setPlan(JSON.parse(planData));
    }
  }, []);

  useEffect(() => {
    processPlanSave();
  }, [mode]);

  const getPrice = (plan: Plan) => {
    return mode === 'month' ? plan.monthPrice : plan.yearPrice;
  };

  const processPlanSave = (newPlanData?: Plan) => {
    if (!newPlanData) return;
    const planData = newPlanData ?? plan;
    const price = getPrice(planData);
    const data = {
      id: planData?.id,
      price: price,
      name: planData.name,
      image: planData.image,
    };
    localStorage.setItem(PLAN_STEP_DATA_KEY, JSON.stringify(data));
  }

  const onCheckPlan = (id: number) => {
    const newPlan = plans.find((p) => p.id === id);
    localStorage.setItem(PLAN_STEP_KEY, JSON.stringify(newPlan));
    processPlanSave(newPlan);
    setPlan(newPlan);
    clearError();
  };

  const clearError = () => {
    setError('');
  }

  const changeMode = () => {
    const newMode = mode === 'month' ? 'year': 'month';
    setMode(newMode);
    localStorage.setItem(PERIOD_MODE_KEY, JSON.stringify(newMode));
  }

  useEffect(() => {
    const newMode = localStorage.getItem(PERIOD_MODE_KEY);
    if (newMode !== mode && newMode) {
      setMode(JSON.parse(newMode));
    }
  }, []);

  const onNext = async () => {
    if (!plan) {
      setError('A step is submitted, but no selection has been made');
      return false;
    }
    clearError();
    return plan && !error;
  }

  const priceRow = (plan: Plan) => {
    const txt = mode === 'month' ? 'mo' : 'yr';
    const price = getPrice(plan);
    return `$${price}/${txt}`;
  }

  return (
    <StepWithControls onNext={onNext}>
      <div>
        <Header
          title={'Select your plan'}
          description={'You have the option of monthly or yearly billing.'}
        />
        <div>
          {error && <Error title={error} />}
          <div className="plans">
            {plans.length > 0 && plans.map((p) => <PlanItem
              key={p.id}
              name={p.name}
              icon={p.image}
              onSelect={() => onCheckPlan(p.id)}
              isSelected={plan?.id === p.id}
              priceRow={priceRow(p)}
            />)}
          </div>
          <div className="period__changer__block">
            <div className="period period--active">Monthly</div>
            <div className="period__switch">
              <label className="period__switch__label">
                <input
                  type="checkbox"
                  className="period__switch__input"
                  checked={mode === 'year'}
                  onChange={changeMode}
                />
                <span className="period__switch__circle"></span>
              </label>
            </div>
            <div className="period">Yearly</div>
          </div>
        </div>
      </div>
    </StepWithControls>
  );
};
