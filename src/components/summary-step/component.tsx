import './styles.scss';
import { useContext, useEffect, useState } from 'react';
import { PeriodMode, PlanData } from '../common/plan/types';
import { AddOnItem } from '../common/addon/types';
import { StepWithControls } from '../common/step-with-controls';
import { Header } from '../common/header';
import { StepsContext } from '../../context';
import { ADDONS_KEY, PERIOD_MODE_KEY, PLAN_STEP_DATA_KEY } from '../../config';

export const StepFour = () => {
  const [plan, setPlan] = useState<PlanData>();
  const [mode, setMode] = useState<PeriodMode>();
  const [addons, setAddons] = useState<AddOnItem[]>([]);
  const [total, setTotal] = useState<number>();
  const { setStep } = useContext(StepsContext);

  const pricePeriodShort = mode === 'month' ? 'mo' : 'yr';

  useEffect(() => {
    const planLC = localStorage.getItem(PLAN_STEP_DATA_KEY) ?? '{}';
    const planData: PlanData = JSON.parse(planLC);
    setPlan({ ...planData })

    const modeLC = localStorage.getItem(PERIOD_MODE_KEY);
    const mode = modeLC ? JSON.parse(modeLC) : 'month';
    setMode(mode);

    const addonsLC = localStorage.getItem(ADDONS_KEY) ?? '[]';
    const addonsItems = JSON.parse(addonsLC);
    setAddons(addonsItems);

    const price = planData.price + addonsItems.reduce((total: number, item: AddOnItem) => total + item.price, 0);
    setTotal(price);
  }, []);

  const nextHandler = async () => {
    return true;
  };

  const onPlanPage = () => {
    setStep(2);
  }

  return (
    <StepWithControls onNext={nextHandler}>
      <Header title={'Finishing up'} description={'Double-check everything looks OK before confirming.'} />
      <div>
        <div className="total__table">
          <div className="total__table__top">
            <div className="left">
              <div className="plan__header">{plan?.name} ({ mode === 'month' ? 'Monthly' : 'Yearly' })</div>
              <div className="change__btn" onClick={onPlanPage}>Change</div>
            </div>
            <div className="right">
              ${plan?.price}/{pricePeriodShort}
            </div>
          </div>
          <div className="total__table__divider"></div>
          <div className="total__table__bottom">
            {addons.length > 0 && addons.map((a) =>
              <div className="total__row" key={a.id}>
                <div className="left">{a.name}</div>
                <div className="right">+{a.price}$/{pricePeriodShort}</div>
              </div>
            )}
          </div>
        </div>
        <div className="total__amount">
          <div className="header">Total (per {mode})</div>
          <div className="price">+${total}/{pricePeriodShort}</div>
        </div>
      </div>
    </StepWithControls>
  );
};
