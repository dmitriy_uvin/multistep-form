import { ReactNode, useState } from 'react';
import { StepsContext } from '../context';

interface StepsProviderProps {
  children: ReactNode;
}

export const StepsProvider = ({ children }: StepsProviderProps) => {
  const [step, setStep] = useState<number>(1);
  const value = { step, setStep };

  return (
    <StepsContext.Provider value={value}>
      {children}
    </StepsContext.Provider>
  );
};