export const LAST_STEP = 4;
export const THANK_YOU_STEP = LAST_STEP + 1;

export const FORM_PASSED_KEY = 'form_passed';
export const INFO_STEP_KEY = 'step_one';
export const PLAN_STEP_KEY = 'step_two';
export const PERIOD_MODE_KEY = 'period_mode';
export const ADDONS_KEY = 'addons';
export const PLAN_STEP_DATA_KEY = 'step_two_plan';