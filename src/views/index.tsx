import { StepOne } from '../components/info-step';
import { StepTwo } from '../components/plan-step';
import { StepThree } from '../components/addons-step';
import { StepFour } from '../components/summary-step';
import { ThankYouStep } from '../components/thank-you';
import { useContext, useEffect } from 'react';
import { useIsMobile } from '../hooks';
import { MobileLayout } from '../components/layout/mobile';
import { DesktopLayout } from '../components/layout/desktop';
import { StepsContext } from '../context';
import { FORM_PASSED_KEY, THANK_YOU_STEP } from '../config';

export const StepView = () => {
  const { step, setStep } = useContext(StepsContext);
  const isMobile = useIsMobile();

  useEffect(() => {
    localStorage.removeItem(FORM_PASSED_KEY);
    const formPassed = JSON.parse(localStorage.getItem(FORM_PASSED_KEY) ?? 'false');
    if (formPassed) {
      setStep(THANK_YOU_STEP);
    }
  }, []);

  const steps = (
    <>
      {step === 1 && <StepOne />}
      {step === 2 && <StepTwo />}
      {step === 3 && <StepThree />}
      {step === 4 && <StepFour />}
      {step === 5 && <ThankYouStep
          position={'top'}
          title={'Thank you!'}
          textAlign={'center'}
          description={'Thanks for confirming your subscription! We hope you have fun using our platform. If you ever need support, please feel free to email us at support@loremgaming.com.'}
      />}
    </>
  );

  if (isMobile) {
    return (
      <MobileLayout>
        {steps}
      </MobileLayout>
    );
  }

  return (
    <DesktopLayout>
      {steps}
    </DesktopLayout>
  );
};
