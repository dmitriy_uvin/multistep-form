import { useEffect } from 'react';
import {
  ADDONS_KEY,
  FORM_PASSED_KEY,
  INFO_STEP_KEY,
  PERIOD_MODE_KEY,
  PLAN_STEP_DATA_KEY,
  PLAN_STEP_KEY
} from '../config';

export const useClearLc = () => {
  useEffect(() => {
    const keys = [
      FORM_PASSED_KEY,
      INFO_STEP_KEY,
      PLAN_STEP_KEY,
      PERIOD_MODE_KEY,
      ADDONS_KEY,
      PLAN_STEP_DATA_KEY
    ];
    for (let key of keys) {
      // console.log(`Remove for key ${key}`);
      localStorage.removeItem(key);
    }
  }, []);
}