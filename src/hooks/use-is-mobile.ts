import { useEffect, useState } from 'react';

export const useIsMobile = () => {
  const [isMobile, setIsMobile] = useState<boolean>(false);

  const resizeHandler = () => {
    setIsMobile(window.innerWidth <= 900);
  }

  useEffect(() => {
    resizeHandler();
    window.addEventListener('resize', resizeHandler);

    return () => window.removeEventListener('resize', resizeHandler);
  }, []);

  return isMobile;
}