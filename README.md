### Tech stack

- Mobile first
- React
- Typescript
- Vite
- Sass
- react-hook-form
- localStorage for saving data